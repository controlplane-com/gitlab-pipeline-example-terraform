# Control Plane - GitLab CI/CD Pipeline Example Using Terraform

This example demonstrates building and deploying an app to Control Plane using Terraform as part of a CI/CD pipeline.

The example is a Node.js app that displays the environment variables and start-up arguments.

Terraform requires the current state be persisted between deployments. The example uses GitLab's <a href="https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html" target="_blank">managed Terraform state backend</a> to store the state file.

This example is provided as a starting point and your own unique delivery and/or deployment requirements will dictate the steps needed in your situation.

## Script Overview

The pipeline script for GitLab is defined in the `.gitlab-ci.yml` file.

The image used (`registry.gitlab.com/gitlab-org/terraform-images/stable:latest`), includes the Terraform CLI.

1. The `variables` block sets the environment variables used by the variables in the Terraform script (prefixed with `TF_VAR_`). Any additional variables should be added here.
2. The `before_script` installs the Control Plane Terraform Provider
3. The script is broken down into `stages`:
   - `init`: Runs the Terraform init command
   - `validate`: Runs the Terraform validate command
   - `build`: Installs the Control Plane CLI and authenticates, builds and pushes the application to the org's private image repository. The pipeline will append the short SHA of the commit as the image tag.
   - `plan`: Run the Terraform plan command. The plan file is saved as an artifact to be used by the apply command.
   - `deploy`: Manually run the Terraform apply command using the plan file from the plan step.

## Terraform Set Up

The example uses GitLab's <a href="https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html" target="_blank">managed Terraform state backend</a> to store the state file. No additional configuration is required.

The Terraform state can be viewed and managed in the GitLab UI. From the project home page, select `Intrastructure` in the left menu then click `Terraform`.


## Control Plane Authentication Set Up 

The Terraform provider and Control Plane CLI require a `Service Account` with the proper permissions to perform actions against the Control Plane API. 

1. Follow the Control Plane documentation to create a Service Account and create a key. Take a note of the key. It will be used in the next section.
2. Add the Service Account to the `superusers` group. Once the pipeline executes as expected, a policy can be created with a limited set of permissions and the Service Account can be removed from the `superusers` group.
   

## Example Set Up

When triggered, the pipeline will execute the stages defined in the file located at `.gitlab-ci.yml`. The plan stage will generate a Terraform plan based on the HCL in the `/terraform/terraform.tf` file. After the plan has been reviewed, the apply stage needs to be manually triggered. The HCL will create/update a GVC and workload hosted at Control Plane. Any committed modifications to the repository will trigger the pipeline.

**Perform the following steps to set up the example:**

1. Fork the example into your own workspace.

2. The following variables are required and must be added as GitLab repository variables.
   
Browse to the variables page by:
- Clicking `Settings` (left menu bar), then click `CI/CD`.
- Click the `Expand` button within the `Variables` section.
 
Add the following variables:
- `CPLN_TERRAFORM_PROVIDER_VERSION`: Control Plane Terraform Provider version to install (current version is 1.0.1).
- `CPLN_ORG`: Control Plane org.
- `CPLN_GVC_NAME`: The name of the GVC.
- `CPLN_WORKLOAD_NAME`: The name of the workload.
- `CPLN_IMAGE_NAME`: The name of the image that will be deployed. The pipeline will append the short SHA of the commit as the tag when pushing the image to the org's private image repository.
- `CPLN_TOKEN` Service Account Key from the previous section (masked). 

3. Review, update, and commit the `.gitlab-ci.yml` file
- If needed, update the pipeline steps to be triggered on specific branch actions (pushes, pull requests, etc.) and environments. The apply stage is triggered manually only after a successful execution of the plan step.

4. The Terraform HCL script is located in the `/terraform` directory. No changes are required to execute the example.

5. Run the pipeline by:
- Click `CI/CD` in the left menu, then click `Pipelines`.
- Click `Run pipeline` in the upper right corner, then click the `Run pipeline` button that is in the center of the page.

6. After reviewing the `plan`, the `apply` stage will need to be manually triggered.

## Running the App

After the pipeline has successfully deployed the application, it can be tested by following these steps:

1. Browse to the Control Plane Console.
2. Select the GVC that was set in the `CPLN_GVC_NAME` variable.
3. Select the workload that was set in the `CPLN_WORKLOAD_NAME` variable.
4. Click the `Open` button. The app will open in a new tab. The container's environment variables and start up arguments will be displayed.

## Upgrading Provider Version

1. Update the `CPLN_TERRAFORM_PROVIDER_VERSION` repository variable with the desired provider version.
2. Update the version property inside the HCL file that contains the `required_providers` declaration block for the `cpln` provider. 
3. The example pipeline runs the command `terraform init -upgrade` which will upgrade the Terraform dependencies (state file, etc.).

Note: If necessary, the provider version can be downgraded.

## Helper Links

Terraform

- <a href="https://www.terraform.io/docs/index.html">Terraform Documentation</a>.

GitLab

- <a href="https://docs.gitlab.com/ee/ci/" target="_blank">GitLab CI/CD Docs</a>.

- <a href="https://docs.gitlab.com/ee/user/infrastructure/" target="_blank">Terraform and GitLab</a>.

- <a href="https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html" target="_blank">Set up GitLab managed Terraform State</a>.

